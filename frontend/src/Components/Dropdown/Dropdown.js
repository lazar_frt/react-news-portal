import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 220,
    marginLeft: "40px"
  },
  labelStyle: {
    color: "white"
  }
}));

export default function Dropdown({onSortSelected}) {
  const classes = useStyles();
  const [sortBy, setSortBy] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setSortBy(event.target.value);
    onSortSelected(event.target.value)
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-controlled-open-select-label" className={classes.labelStyle}>Sort By</InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={sortBy}
          onChange={handleChange}
          className={classes.labelStyle}
          >
          <MenuItem value="popularity">Popularity</MenuItem>
          <MenuItem value="relevance">Relevance</MenuItem>
          <MenuItem value="publishedAt">Published At</MenuItem>
        </Select>
      </FormControl>
  );
}
