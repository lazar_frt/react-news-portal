import React,{useCallback} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Dropdown from '../Dropdown/Dropdown'
import { Button } from '@material-ui/core'
import {Link} from 'react-router-dom'
import './Nav.css'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      color: "white"
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '30em',
      '&:focus': {
        width: '40em',
      },
    },
  },
}));

export default function Navbar({ onInputChange, onSearchClick, onSortSelected, isArticle }) {
  const classes = useStyles();



  const onClick = useCallback(
    () => !isArticle && window.location.reload(),
    [isArticle],
  );

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className="header-layout">
          <Button color="in" onClick={onClick}>
            <Link to="/" className="link-test-deco">
            <Typography className={classes.title} variant="h6" noWrap>
              Latest News
            </Typography>
            </Link>
          </Button>
          {!isArticle && <div className={classes.search}>
            <IconButton
              color="inherit"
              onClick={onSearchClick}
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              onChange={onInputChange}
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>}
          {!isArticle  && <Dropdown onSortSelected={onSortSelected} />}
        </Toolbar>
      </AppBar>
    </div>
  );
}
