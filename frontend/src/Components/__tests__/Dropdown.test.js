import * as React from 'react'
import * as ReactDOM from 'react-dom'

import Dropdown from '../Dropdown/Dropdown'

test('Should render label text correctly', () => {
    const root = document.createElement("div");
    ReactDOM.render(<Dropdown />, root);
    expect(root.querySelector("label").textContent).toBe("Sort By");
});