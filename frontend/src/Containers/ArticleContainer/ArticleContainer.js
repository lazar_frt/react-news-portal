import React from 'react'
import ArticleContent from './ArticleContent';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';
import moment from 'moment'


function ArticleContainer() {
    const {id} = useParams();
    const articles = useSelector(state => state.news.items);
    const article = articles.find( article => article.title === `${id}`);
    const formatedDate = article ? moment(article.publishedAt).format('LLLL') : '';
    return (
        <>
         {article  ? <ArticleContent
         title={article.title} 
         source={article.source.name}
         author={article.author}
         description={article.description}
         imageUrl={article.urlToImage}
         publishedAt={formatedDate}
         /> : <h3>Article not found</h3>}
        </>
    );
}

export default ArticleContainer