import React from 'react'
import { Typography, Container } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import Navbar from '../../Components/Navbar/Navbar'
import './Article.css';

function ArticleContent({
  title,
  source,
  author,
  description,
  imageUrl,
  publishedAt
}) {
  return (
    <>
    <Navbar isArticle={true} />
      <Container fullWidth>
        <CardActionArea>
          <Typography gutterBottom variant="h3" component="h1" className="title-style">
            {title}
          </Typography>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="700"
            image={imageUrl}
            title="Contemplative Reptile"
            className="image-style"
          />
          <CardContent>
            <Typography variant="h5" color="textSecondary" component="p">
              {description}
            </Typography>
          </CardContent>
        </CardActionArea>
        <Typography variant="body2" color="textSecondary" component="p">
          Source: {source}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Author: {author}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Published at: {publishedAt}
        </Typography>
      </Container>
    </>
  );
}

export default ArticleContent;

