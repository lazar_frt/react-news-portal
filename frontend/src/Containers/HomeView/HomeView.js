import Navbar from '../../Components/Navbar/Navbar'
import useSearch from '../../hooks/useSearch';
import LandingContainer from '../LandingContainer/LandingContainer'

function HomeView() {
    const {onInputChange,onSearchClick, onSortSelected} = useSearch();

    return (
        <>
            <Navbar onInputChange={onInputChange} onSearchClick={onSearchClick} onSortSelected={onSortSelected}/>
            <LandingContainer/>
        </>
    );
}

export default HomeView;
