import useLandingPageData from '../../hooks/useLandingPageData';
import MediaCard from '../../Components/MediaCard/MediaCard';
import GridList from '@material-ui/core/GridList';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import './landing.css'

const { REACT_APP_DEFAULT_IMG } = process.env;

function LandingContainer() {
    const { articles, loadMore, loading, error } = useLandingPageData();

    const newsCards = articles && articles.map((article, index) => {
        if (!article.urlToImage) article.urlToImage = REACT_APP_DEFAULT_IMG
        return <MediaCard key={index} title={article.title} description={article.description} imgUrl={article.urlToImage} fullArticle={article} />
    })

    return (
        <>
            {error && <Typography color="error" variant="h2" align="center">{error}</Typography>}
            {loading && (<CircularProgress size="8em" className="circular-progr-center" />)}
            {!loading && <GridList className="card-ul-margin">
                {newsCards}
            </GridList>}
            {!loading &&
                (
                    <div className="button-load">
                        <Button
                            className="butt-width"
                            variant="outlined"
                            color="primary"
                            onClick={loadMore}
                        >
                            Load more
                        </Button>
                    </div>
                )}
        </>
    );
}

export default LandingContainer;

