import ArticleContainer from './Containers/ArticleContainer/ArticleContainer'
import HomeView from './Containers/HomeView/HomeView';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


function App() {
  return (
      <Router>
        <Switch>
        <Route path='/' exact component={HomeView} />
        <Route path='/article/:id' exact component={ArticleContainer}/>
        </Switch>
      </Router>
  );
}

export default App;
