import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import {persistStore} from 'redux-persist'
import logger from 'redux-logger'

const initialState = {};

const middleware = [thunk,logger];

export const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middleware)
);

export const persistor = persistStore(store);

export default {store,persistor};