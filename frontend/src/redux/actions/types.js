//TOP HEADLINES NEWS
export const FETCH_TOP_HEADLINES_REQUEST = "FETCH_TOP_HEADLINES_REQUEST";
export const FETCH_TOP_HEADLINES_FAILURE = "FETCH_TOP_HEADLINES_FAILURE";
export const FETCH_TOP_HEADLINES_SUCCESS = "FETCH_TOP_HEADLINES_SUCCESS";


//EVERYTHING NEWS TYPES
export const FETCH_EVERYTHING_NEWS_REQUEST = "FETCH_EVERYTHING_NEWS_REQUEST"
export const FETCH_EVERYTHING_NEWS_FAILURE = "FETCH_EVERYTHING_NEWS_FAILURE"
export const FETCH_EVERYTHING_NEWS_SUCCESS = "FETCH_EVERYTHING_NEWS_SUCCESS"

