import axios from 'axios'
import {
    FETCH_TOP_HEADLINES_SUCCESS,
    FETCH_TOP_HEADLINES_FAILURE,
    FETCH_TOP_HEADLINES_REQUEST,
    FETCH_EVERYTHING_NEWS_REQUEST,
    FETCH_EVERYTHING_NEWS_FAILURE,
    FETCH_EVERYTHING_NEWS_SUCCESS
} from './types'


const {
    REACT_APP_NEWS_API_KEY:apiKey,
    REACT_APP_TOP_HEADLINES_NEWS_API,
    REACT_APP_EVERYTHING_NEWS_API
} = process.env;

export const fetchTopHeadlinesNews = (pageSize=20) => {
    return (dispatch) => {
        dispatch({
            type: FETCH_TOP_HEADLINES_REQUEST
        })
        axios.get(`${REACT_APP_TOP_HEADLINES_NEWS_API}?country=us&apiKey=${apiKey}&pageSize=${pageSize}`)
        .then(response => {
            dispatch({
                type: FETCH_TOP_HEADLINES_SUCCESS,
                payload: response.data.articles
            })
        })
        .catch(error => {
            const errorMsg = error.message
            dispatch({
                type: FETCH_TOP_HEADLINES_FAILURE,
                payload: errorMsg
            })
        })
    }
}

export const fetchEverythingNews = ({searchTerm,sortBy}) => {
    const params = {
        apiKey,
        qInTitle: searchTerm || 'tesla',
        sortBy
    }
    return (dispatch) => {
        dispatch({
            type: FETCH_EVERYTHING_NEWS_REQUEST
        })
        axios.get(`${REACT_APP_EVERYTHING_NEWS_API}`,{params})
        .then(response => {
            dispatch({
                type: FETCH_EVERYTHING_NEWS_SUCCESS,
                payload: response.data.articles
            })
        })
        .catch(error => {
            const errorMsg = error.message
            dispatch({
                type: FETCH_EVERYTHING_NEWS_FAILURE,
                payload: errorMsg
            })
        })
    }
}
