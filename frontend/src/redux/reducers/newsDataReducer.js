import {
    FETCH_TOP_HEADLINES_SUCCESS,
    FETCH_TOP_HEADLINES_FAILURE,
    FETCH_TOP_HEADLINES_REQUEST,
    FETCH_EVERYTHING_NEWS_REQUEST,
    FETCH_EVERYTHING_NEWS_FAILURE,
    FETCH_EVERYTHING_NEWS_SUCCESS
} from '../actions/types'

const initialState = {
    items: [],
    loading: false,
    error: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_TOP_HEADLINES_REQUEST:
            return {
                ...state,
                loading: true
            }
        case FETCH_TOP_HEADLINES_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload,
                error: ''
            }
        case FETCH_TOP_HEADLINES_FAILURE:
            return {
                ...state,
                loading: false,
                items: [],
                error: action.payload
            }

        case FETCH_EVERYTHING_NEWS_REQUEST:
            return {
                ...state,
                loading: true
            }
        case FETCH_EVERYTHING_NEWS_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload,
                error: ''
            }
        case FETCH_EVERYTHING_NEWS_FAILURE:
            return {
                ...state,
                loading: false,
                items: [],
                error: action.payload
            }
        default:
            return state;
    }
}