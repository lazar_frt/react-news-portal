import { combineReducers } from "redux";
import {persistReducer} from 'redux-persist';
import newsDataReducer from "./newsDataReducer";
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['news']
}
const rootReducer = combineReducers({
  news: newsDataReducer,
});


export default persistReducer(persistConfig,rootReducer);