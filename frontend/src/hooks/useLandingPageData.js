import { useEffect,useState,useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchTopHeadlinesNews } from "../redux/actions/newsDataActions";

export default function useLandingPageData() {
  const [pageSize,setSize] = useState(20);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchTopHeadlinesNews(pageSize));
  }, [dispatch,pageSize]);

  const {error,items,loading} = useSelector(state => state.news);
  
  const loadMore = useCallback(
    () => {
      setSize(prevValue => prevValue + 20);
    },
    [],
  );

  return {
    articles: items || [],
    loadMore,
    loading: loading,
    error
  };
}