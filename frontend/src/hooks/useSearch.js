import { useState } from "react";
import { useDispatch } from "react-redux";
import { fetchEverythingNews } from "../redux/actions/newsDataActions";

export default function useSearch() {
    const dispatch = useDispatch();
    const [searchTerm,setSearhTerm] = useState('');
    
    const onInputChange = (e) => {
        setSearhTerm(e.target.value);
    }
    const onSearchClick = () => {
        dispatch(fetchEverythingNews({searchTerm}));
    }

    const onSortSelected = (sortBy) => {
        dispatch(fetchEverythingNews({sortBy}))
    }
  return {
      onInputChange,
      onSearchClick,
      onSortSelected
  };
}