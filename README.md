# React News portal app

## Installation

Clone the repo:
```
git clone https://lazar96f@bitbucket.org/lazar96f/react-news-portal.git
```

## How to start project:
- Set your API Key from free news api into .env.development file
```
cd /frontend
npm install
npm start
```

## Description:

- Redux for state managment
- React-router
- Material-UI library
- [API used for data](https://newsapi.org/)
- Environment variables path /frontend/.env.development


## Tests

```
npm run test
```